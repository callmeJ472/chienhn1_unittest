from src.transformF2C import deg_F2deg_C

def test_deg_F2deg_C():
    assert int(deg_F2deg_C(100)) == 37

def test_deg_F2deg_C_1():
    assert int(deg_F2deg_C(100)) != 38

def test_deg_F2deg_C_wrong_data_type():
    assert int(deg_F2deg_C(100)) != '37'
